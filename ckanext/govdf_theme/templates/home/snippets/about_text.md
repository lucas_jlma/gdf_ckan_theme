## O que são dados abertos?
Segundo a Fundação do Conhecimento Aberto *(Open Knowledge Foundation – OKF)*:
“dados são abertos quando qualquer pessoa pode livremente usá-los, reutilizá-los e redistribuí-los, estando sujeito a, no máximo, a exigência de creditar a sua autoria e compartilhar pela mesma licença”.
Saiba mais sobre Dados Abertos. [Clique aqui](/dados-abertos).
## O que é o dados.df.gov.br?
O Portal de Dados Abertos do Distrito Federal é a ferramenta disponibilizada pelo governo para que todos possam encontrar e utilizar os dados e as informações públicas sobre diversos temas em formato bruto e aberto. Ele é um serviço simplificado que organiza e padroniza o acesso aos dados públicos, primando pelo reuso desses dados e o uso de tecnologias modernas.
Funciona como um grande catálogo que facilita a busca e uso de dados publicados pelos órgãos e entidades do Poder Executivo Distrital, permitindo sua reutilização em aplicações digitais desenvolvidas pela sociedade.
O portal preza pela simplicidade e organização para que você possa encontrar facilmente os dados e informações que precisa. Também tem o objetivo de promover a interlocução entre atores da sociedade com o governo, para pensar na melhor forma de utilização em benefício da sociedade.
O objetivo e o conteúdo do Portal de Dados Abertos diferem do [Portal da Transparência do Distrito Federal](http://www.transparencia.df.gov.br), que disponibiliza informações pormenorizadas sobre a execução orçamentária e financeira do Governo do Distrito Federal, com o objetivo de aumentar o controle social sobre as despesas e receitas do governo.

## Quais dados estão disponíveis aqui?
O portal tem o objetivo de disponibilizar o maior número de dados e informações possível. Como por exemplo, dados da saúde, do sistema de transporte, de segurança pública, indicadores de educação, gastos governamentais, dentre outros.
Neste primeiro momento, o portal disponibiliza o acesso a uma parcela dos dados publicados pelo governo e passará por um processo evolutivo constante para que o volume de dados e informações relevantes seja crescente. 

## O que dados abertos tem a ver com você?
O acesso à informação é um direito previsto na Constituição Federal e compreende, entre outros, o direito de obter informação primária, íntegra, autêntica e atualizada. 
Um dos aspectos previstos na Lei Distrital de Acesso à Informação – LAI/DF, nº 4.990/2012, é a adoção de meios eletrônicos para a disponibilização de dados públicos, que devem ser publicados de forma que facilite sua reutilização e que permita o acesso simplificado para os seus usuários, permitindo que a sociedade desenvolva aplicativos digitais com essas informações.
A abertura de dados governamentais promove a atuação conjunta entre indivíduos, organizações e a administração pública, produzindo algo de valor para a sociedade, seja para aumentar a transparência, facilitar a prestação de contas, melhorar a qualidade dos serviços públicos, como também para a criação de empregos.

## Por que estamos fazendo isso?
Em 12 de dezembro de 2012 foi sancionada a [Lei Distrital de Acesso à Informação - LAI](http://www.presidencia.gov.br/ccivil_03/_Ato2011-2014/2011/Lei/L12527.htm) (Lei 4.990/2012) que regula o acesso a dados e informações dos órgãos públicos. A LAI constitui um marco para a democratização da informação pública, e preconiza, dentre outros requisitos técnicos, que a informação solicitada pelo cidadão deve seguir critérios tecnológicos alinhados com as “3 leis de dados abertos”:
1.  Se o dado não pode ser encontrado e indexado na Web, ele não existe;
2.  Se não estiver aberto e disponível em formato compreensível por máquina, ele não pode ser reaproveitado; e
3.  Se algum dispositivo legal não permitir sua replicação, ele não é útil.

Sendo assim, o Portal de Dados Abertos do Distrito Federal é a ferramenta utilizada pelo governo para centralizar a busca e o acesso dos dados e informações dos órgãos públicos, visando o contínuo aprimoramento da transparência na gestão.

## Como o portal foi criado?
Para a criação do Portal de Dados Abertos do Distrito Federal foi utilizada a aplicação web de catalogação de dados - [CKAN (*Comprehensive Knowledge Archive Network*)](http://ckan.org), desenvolvida pela Open Knowledge Foundation. 
A alimentação e adequação do conteúdo foi realizada por servidores da Secretaria de Estado de Planejamento, Orçamento e Gestão – SEPLAG e as customizações foram implementadas pela Secretaria de Comunicação – SECOM. A gestão da política de Dados Abertos do Governo de Brasília será realizada pela Controladoria-Geral do Distrito Federal.
